import sys
import logging
import click
import time
import requests

logging.basicConfig()
logging.root.setLevel(logging.INFO)
logger = logging.getLogger(__name__)

GITLAB_API = 'https://gitlab.com/api/v4'


@click.command()
@click.option("-proj", "--project", type=str, required=True, help="id of the project where the pipeline lives")
@click.option("-pipe", "--pipeline", type=str, required=True, help="if of the pipeline to be monitored and retried")
@click.option("-tkn", "--token", type=str, required=True, help="your gitlab private token")
@click.option("-int", "--interval", type=int, default=10, help="the interval between retries")
@click.option("-max", "--max-retries", type=int, default=20, help="maximum number of retries")
def main(project, pipeline, token, interval, max_retries):

    url = f'{GITLAB_API}/projects/{project}/pipelines/{pipeline}'
    headers = {'PRIVATE-TOKEN': token}

    pipeline_status = None
    retry = 0

    while pipeline_status != 'success' and retry < max_retries:
        logger.error(f'Fetching pipeline [{pipeline}] from project [{project}]')

        try:
            response = requests.get(url=url, headers=headers)
            response.raise_for_status()

        except Exception as error:
            logger.error(f'Failed to fetch pipeline! - {error}')
            exit()

        data = response.json()
        pipeline_status = data['status']

        logger.info(f'Pipeline [{pipeline}] status: {pipeline_status}')

        # if pipeline failed, retry
        if pipeline_status == 'failed':
            logger.info(f'Pipeline [{pipeline}] has failed! Retrying...   (retry number {retry})')

            retry_url = f'{url}/retry'

            try:
                response = requests.post(url=retry_url, headers=headers)
                response.raise_for_status()

            except Exception as error:
                logger.error(f'Failed to retry pipeline! - {error}')
                exit()

            retry = retry + 1

        # fetch the pipeline again after an interval
        time.sleep(interval)

    logger.info(f'Pipeline [{pipeline}] was sucessfully completed!')


if __name__ == "__main__":
    try:
        main()

    except KeyboardInterrupt:
        logger.info('Exiting...')
        sys.exit(0)
