# Gitlab Pipeline Retrier

This tool can be used in order to monitor a project pipeline and automatically retry it if it fails.

The Gitlab API is used, which requires your private token in order to have access to the projects.

The pipeline is fetched and if the status is `failed` it is retried. The script then waits some time and fetches the pipeline again.

The script runs until the pipeline status is `success` or a maximum number of retries if reached.


* * *

### Running the tool

#### Parameters:
**`-proj`|`--project`**: id of the project. Can be checked under `Project->Settings->General`

**`-pipe`|`--pipeline`**: id of the pipeline to monitor;

**`-tkn`|`--token`**: your gitlab private token;

**`-int`|`--interval`**: interval between requests (default 10sec);

**`-max`|`--max-retries`**: maximum number of retries (default 20).

<br>

#### Command
```
python retrier.py -proj a9auxi9ax -pipe 10osmkasd -tkn 101oje21s
````